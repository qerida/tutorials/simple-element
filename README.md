# simple-element

How to write a simple element and display it in a browser.

# Usage

- Pull the repo: `git clone https://gitlab.com/qerida/tutorials/simple-element.git`
- Install modules: `npm install`
- Start the server: `polymer serve` or `npm start` 

# Explanation file by file

## .gitignore

This file lists files and folders that should not be added to the git repository. Often these are build-files, installed modules or temporary files.

- `.idea/`: A folder for JetBrains IDEs settings (WebStorm). Not relevant to the project.
- `node_modules/`: The place where npm installs the podules specified in package.json -> dependencies
- `package-lock.json`: Check out the [docs](https://docs.npmjs.com/files/package-lock.json). Personally I dont like it when working on private repos, so I always leave it out. Though it is recommended to include it.

## index.html and demo/index.html

By default the index.html in the top level folder is the entrypoint for fileservers (`polymer serve`). The index.html in the top level folder redirects to the index.html in the demo folder. When developing multiple custom elements in one repo the demo folder stores an html file per element. demo/index.html shows how to include a custom element in an html file.

- The first script tag includes a script to load polyfills for webcomponents
- The second script tag includes our custom element
- In the body the custom element is used: `<simple-element></simple-element>`

## package.json

Check out the full documentation [here](https://docs.npmjs.com/files/package.json). The package.json in this repo is self-explanatory. I only describe the dependencies.

- `"@polymer/lit-element": "^0.5.0"`: Provides the base class for using Polymer with lit-html
- `"@webcomponents/webcomponentsjs": "^2.0.0-beta.2"`: Provides the polyfill loader used in the html files.

The key of the dependency object is the package name. The value contains a version specifier. 

Notice that the two dependencies are namespaced (`@`). Not all npm packages are. E.g. The namespace for the first package is `@polymer`.

You can find the packages on [npm](https://www.npmjs.com/). I always recommend looking up the packages you work with. Often you find the github repository linked. Browse it! It is always interesting how other people build their packages. The GitHub repo is especially interesting for the Changelog or Issues if something broke.

## simple-element.js

Here the simple element is created. The code does not give much to explain so look at the comments and ask if you have any questions.
